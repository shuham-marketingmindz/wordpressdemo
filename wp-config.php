<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '12345' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '+ Y:RUeE7ccUFaW^k??HMQVvg12k6B58yYMZ]WS93vYv}|X-yWe9@iShJ&eEg[w^' );
define( 'SECURE_AUTH_KEY',  '2XX_*[K;hRfg=e_XJ>q25#F=7;XWu7Bg/ex$Rc^[QGUD%c=y?5q`OiCLe&I%oiT|' );
define( 'LOGGED_IN_KEY',    'uBFrvCP=G1J9C)ptnwFe}OWJB7a~%xm#I#-`57[Y9hkjK$/sOYGXft!S:iuB1|4>' );
define( 'NONCE_KEY',        'HM#fo?Q@@dRR `{>gJ0g~{`09a<uK (#8GMhL$y| kvoR[|8=~C[D}S6KCmD]l,u' );
define( 'AUTH_SALT',        '<u={`8 +Et{]_gse%Su#>#7H2Ye;i]^}/U6QQ3S&Lq4-[=iPRt<~uTeoMD$`W|ya' );
define( 'SECURE_AUTH_SALT', '//jP4mHmmlzE<9S$2_H%7qMbeh-}shUWd%rvHq99:=-%C02]?~|MWD=-ZW,S{p%[' );
define( 'LOGGED_IN_SALT',   'ct{k_l.fcN{Pxpjpbs~FHPi4mTINBu,0<**n{!63[CKe:(hbW.%HIFB!Ty,A/n}d' );
define( 'NONCE_SALT',       't@>U.}d$G.!P|igSw$7q(gHlo~KNADzM?OAg7&=)Cesldu)A5ESQ#pH9?(sf|tJU' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
